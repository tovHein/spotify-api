var mainContainer = document.getElementById('js-main-container'),
mainContainerPlaylist = document.getElementById('js-main-container-playlist'),
loginContainer = document.getElementById('js-login-container'),
loginButton = document.getElementById('js-btn-login'),
background = document.getElementById('js-background');

var spotifyPlayer = new SpotifyPlayer({
  exchangeHost: 'http://192.168.1.4:5000'
});

var trackId;

var playerOutput = function (data) {
  trackId = data.item.album.id;
  return `
  <div class="main-wrapper">
  <img class="now-playing__img" src="${data.item.album.images[0].url}">
  <div class="now-playing__side">
  <div class="now-playing__name">${data.item.name}</div>
  <div class="now-playing__artist">${data.item.artists[0].name}</div>
  <div class="now-playing__status">${data.is_playing ? 'Playing' : 'Paused'}</div>
  <div class="progress">
  <div class="progress__bar" style="width:${data.progress_ms * 100 / data.item.duration_ms}%"></div>
  </div>
  </div>
  </div>
  <div class="background" style="background-image:url(${data.item.album.images[0].url})"></div>
  `;
};

spotifyPlayer.on('update', response => {
  mainContainer.innerHTML = playerOutput(response);
});

var playlistOutput = function (data) {
  var tempOutput = `<table>
  <tr class="line"><th> No. </th><th> Song Title </th><th> Artist </th></tr>
  `;
  Object.keys(data.tracks.items).forEach(function (item) {
    var counter = parseInt(item);
    tempOutput += `<tr id="${data.tracks.items[item].track.album.id}" class="list">
    <td> ${counter + 1}  <?xml version="1.0" encoding="UTF-8" standalone="no"?>
    <svg width="16px" height="16px" viewBox="0 0 16 16" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
    <defs></defs>
    <g id="icon-equalizer-anim" fill="#4A4A4A">
    <rect class="eq__bar" id="eq1" x="1" y="8" width="4" height="8"></rect>
    <rect class="eq__bar" id="eq2" x="6" y="1" width="4" height="15"></rect>
    <rect class="eq__bar" id="eq3" x="11" y="4" width="4" height="12"></rect>
    </g>
    </svg>  </td>
    <td> ${data.tracks.items[item].track.album.name} </td>
    <td> ${data.tracks.items[item].track.album.artists[0].name} </td>
    </tr>` ;
  });
  tempOutput += `</table>`; 
  return tempOutput;
};

spotifyPlayer.on('ready', responsePlaylist => {
  mainContainerPlaylist.innerHTML = playlistOutput(responsePlaylist);
  addClass(document.getElementById(trackId),'active');
});


spotifyPlayer.on('login', user => {
  if (user === null) {
    loginContainer.style.display = 'block';
    mainContainer.style.display = 'none';
  } else {
    loginContainer.style.display = 'none';
    mainContainer.style.display = 'block';
  }
});

loginButton.addEventListener('click', () => {
  spotifyPlayer.login();
});

spotifyPlayer.init();

function addClass(elements, myClass) {
  // if there are no elements, we're done
  if (!elements) { return; }
  // if we have a selector, get the chosen elements
  if (typeof(elements) === 'string') {
    elements = document.querySelectorAll(elements);
  }
  // if we have a single DOM element, make it an array to simplify behavior
  else if (elements.tagName) { elements=[elements]; }
  // add class to all chosen elements
  for (var i=0; i<elements.length; i++) {
    // if class is not already found
    if ( (' '+elements[i].className+' ').indexOf(' '+myClass+' ') < 0 ) {
      // add class
      elements[i].className += ' ' + myClass;
    }
  }
}

function removeClass(elements, myClass) {
  // if there are no elements, we're done
  if (!elements) { return; }
  // if we have a selector, get the chosen elements
  if (typeof(elements) === 'string') {
    elements = document.querySelectorAll(elements);
  }
  // if we have a single DOM element, make it an array to simplify behavior
  else if (elements.tagName) { elements=[elements]; }
  // create pattern to find class name
  var reg = new RegExp('(^| )'+myClass+'($| )','g');
  // remove class from all chosen elements
  for (var i=0; i<elements.length; i++) {
    elements[i].className = elements[i].className.replace(reg,' ');
  }
}